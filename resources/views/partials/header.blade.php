<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
	 <meta name="dicoding:email" content="chachanurholis29@gmail.com">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>BasmiCorona</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="" />

	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
	
	<link rel="icon" type="png/jpg" href="{{ asset('assets/icons/indonesia.png') }}">
	<!-- Animate.css -->
	<link rel="stylesheet" href="{{ asset('assets/healthcare/css/animate.css') }}">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="{{ asset('assets/healthcare/css/icomoon.css') }}">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="{{ asset('assets/healthcare/css/bootstrap.css') }}">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="{{ asset('assets/healthcare/css/magnific-popup.css') }}">
	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="{{ asset('assets/healthcare/css/owl.carousel.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/healthcare/css/owl.theme.default.min.css') }}">
	<!-- Flexslider  -->
	<link rel="stylesheet" href="{{ asset('assets/healthcare/css/flexslider.css') }}">
	<!-- Flaticons  -->
	<link rel="stylesheet" href="{{ asset('assets/healthcare/fonts/flaticon/font/flaticon.css') }}">
	<!-- Theme style  -->
	<link rel="stylesheet" href="{{ asset('assets/healthcare/css/style.css') }}">

	<!-- Modernizr JS -->
	<script src="{{ asset('assets/healthcare/js/modernizr-2.6.2.min.js') }}"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	<!-- Modernizr JS -->
	<script src="{{ asset('assets/healthcare/js/modernizr-2.6.2.min.js') }}"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
		
	<div class="colorlib-loader"></div>
	
	<div id="page">
	<nav class="colorlib-nav" role="navigation">
		<div class="top-menu">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="top">
							<div class="row">
								<div class="col-md-6">
									<div id="colorlib-logo"><a href="{{ url('') }}">basmi<span>corona</span></a></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="menu-wrap">
				<div class="container">
					<div class="row">
						<div class="col-xs-8">
							<div class="menu-1">
								<ul>
									<li class="active"><a href="#">Home</a></li>
									<li><a href="#">Data</a></li>
									<li><a href="#">Berita</a></li>
									<li><a href="#">Contact</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</nav>